import telebot
from time import sleep
from telebot import types
#Получаем токен у бота BotFather
token = '6396720277:AAH_D6N3RsFSGsq9AV-BxECzm9cskwqC6w0'
bot = telebot.TeleBot(token)

@bot.message_handler(commands=['start'])
def send_welcome(message, res=False):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKzDJlXeaFBxIgaPbhFLoa-yNjvNkotQAC8zMAAjE98Upj_2I0m8NOzzME")
    welcome_text = 'Приветствую, друзья! Я рад приветствовать вас в нашем чате, посвященном точкам сбора мусора в Югре. Мы собрались здесь с целью сделать наш регион более чистым и здоровым, и каждый из нас играет важную роль в этом процессе.'
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True, one_time_keyboard = False)
    button1 = types.KeyboardButton("Пунткы приема")
    button2 = types.KeyboardButton("Обучающие материалы")
    button3 = types.KeyboardButton("5 причин сортировать отходы")
    button4 = types.KeyboardButton("Виды сырья")
    button5 = types.KeyboardButton("Полезные ссылки")
    button6 = types.KeyboardButton("Эко урок")
    keyboard.add(button1,button2,button3,button4,button5,button6)
    bot.send_message(message.chat.id,  welcome_text, reply_markup=keyboard)


@bot.message_handler(commands=['khanty'])
def send_khanty(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy7tlXdK76y4KrMsc2ut9AAGlFSAfgogAAtk5AAK0zfBK8AUy0j5AJkUzBA")
    khanty_text = "Пункт приема в Ханты-Мансийске"
    bot.send_message(message.chat.id, khanty_text)
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    button_url = types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/ecocenters')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Находится в г. Ханты-Мансийск ул. Чехова, д. 74.' 'Номер телефона: 8 800 222 11 86', reply_markup=keyboard)

@bot.message_handler(commands=['surgut'])
def send_surgut(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy79lXdLSQg3Pb3e0CHoYHRE9VtNPaQACOzIAAoY_6UoQ-RdhH7GNtTME")
    surgut_text = "Пункт приема в Сургуте"
    bot.send_message(message.chat.id, surgut_text)
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    button_url = types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/ecocenters')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Нахотися в г. Сургут ул. 30 лет Победы, д. 74.' 'Номер телефона: 8 800 222 11 86', reply_markup=keyboard)

@bot.message_handler(commands=['nizhnevartovsk'])
def send_nizhnevartovsk(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy71lXdLKHEecSEZHSL04iozaFxejHgACzTMAAvhW8UoP0cqMgBY0pTME")
    nizhnevartovsk_text = "Пункт приема в Нижневартовске"
    bot.send_message(message.chat.id, nizhnevartovsk_text)
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    button_url = types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/ecocenters')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Нахотися в г. Нижневартовск ул. Комсомольское озеро, д. 2.' 'Номер телефона: 8 800 222 11 86', reply_markup=keyboard)

@bot.message_handler(commands=['flourpaper'])
def send_flourpaper(message):
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy89lXdMvOG8WlAI_ERwHO5VUfCjRfAACGzMAAlWY8Eqi2Wq5dJhQDTME")
        flourpaper_text = 'ПРАВИЛА ПРИЁМА:' '✅Книги, газеты, журналы, бумага, гофрокартон' '✅Чистое, сложенное в коробки или перевязанное, без жирных пятен и остатков еды, без файлов и скрепокㅤНЕ ПРИНИМАЕТСЯ:' 'ㅤ❌Одноразовая посуда, обои, чеки, пачки сигарет, мешки от строительных смесей, упаковка из пульперкартона, гофроупаковка от яиц'
        bot.send_message(message.chat.id, flourpaper_text)

@bot.message_handler(commands=['glass'])
def send_glass(message):
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8tlXdMr5iKaDrE7PJ_gPJr7K9u6jQACdkcAAgy-8Ep3CvuN8e6hwjME")
        glass_text  = 'Коричневые, зеленые и бесцветные стеклянные бутылки и банки'  'ПРАВИЛА ПРИЁМА:'  '✅Чистые, без крышек и ободков, бумажные этикетки можно не убирать ✅Необходимо сдавать по отдельности коричневое, зеленое и бесцветное стекло'
        bot.send_message(message.chat.id, glass_text)

@bot.message_handler(commands=['plastic'])
def send_plastic(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy9NlXdMzZwulxEnl4oc3Wzc--UiDIAAC_TwAAlUJ8Up8AZt5RgYMWDME")
    plastic1_text = "Пластиковая тара ПНД от бытовой химии" "ПРАВИЛА ПРИЁМА: ✅Чистые, любого цвета ✅Необходимо снять дозаторыㅤ НЕ ПРИНИМАЕТСЯ:❌Тара от удобрений, автохимии и без маркировки"
    bot.send_message(message.chat.id, plastic1_text)
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    button_url = types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/ecocenters')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Ознакомиться со всеми видами сырья можно тут', reply_markup=keyboard)


@bot.message_handler(commands=['reason'])
def send_reason(message):
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8FlXdMg7_rHoM-DAAESGHvGhesfB_YAAmU2AAIxKPFK924bE4GiXFMzBA")
        reason1_text  = 'Экономия природных ресурсов'
        bot.send_message(message.chat.id, reason1_text)
        sleep(3)
        
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8NlXdMjv1rceBRG_lD8ObXsObOtHwACHkcAAg1F6UrN3IYHqEMRpDME")
        reason2_text  = 'Уменьшение объемов отходов: размещаемых на полигонах'
        bot.send_message(message.chat.id, reason2_text)
        sleep(3)
        
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8VlXdMlxqlKdH59w85SEIUhwgiX7wACdjcAAuK18UqKELm93bVyMjME")
        reason3_text  = 'Повторное использование ресурсов и снижение затрат на производство новых изделий'
        bot.send_message(message.chat.id, reason3_text)
        sleep(3)
        
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8dlXdMnsAx2NMnwkmTkfVzO8l6nuQACFjsAAifx8UroxiydOmoJXTME")
        reason4_text  = 'Бонус за сдачу правильно подготовленного вторичного сырья'
        bot.send_message(message.chat.id, reason4_text)
        sleep(3)

        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy8llXdMphlaZITuHymZsywPH7fPJeQACMTcAAqP_8Eq-iGLUWtNstTME")
        reason5_text  = 'Повышение уровня сознательности'
        bot.send_message(message.chat.id, reason5_text)
        sleep(3)


@bot.message_handler(commands=['ecolesson'])
def send_lesson(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKzDtlXfITDNwU31L_WqzsE4-N2c6UoAACnj0AAvuS8EqCz2sleJcFojME")
    poem_text1 = "Записаться на посещение эклогического урока "
    poem_text2 = "В павильоне наглядно показано какие виды отходов перерабатываются или нет, какая альтернатива есть одноразовым пластиковым вещам и что изготавливается из вторичного сырья. Посетители экоуроков узнают о раздельном сборе отходов и дальнейшем пути вторичного сырья из первых рук."
    bot.send_message(message.chat.id, poem_text2)
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    button_url = telebot.types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/form')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Обучающие материалы', reply_markup=keyboard)

@bot.message_handler(commands=['educationalplatform'])
def send_platform(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKzGtlXfSb3WOZksJsUafjISklB5SGfAAC3jsAAlto8Er9y4es_b7emjME")
    poem_text2 = "Экоцентр «Югра Собирает» не только пункт приема вторсырья - это еще и образовательная площадка, на которой проходят лекции. "
    bot.send_message(message.chat.id, poem_text2)
    #Создаем кнопку, которая появится только после сообщения и будет хранить ссылку на сайт
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    button_url = telebot.types.InlineKeyboardButton("Перейти", url='https://sobiraet.yugra-ecology.ru/training')
    keyboard.add(button_url)
    bot.send_message(message.chat.id,  'Обучающие материалы', reply_markup=keyboard)


@bot.message_handler(commands=['usefullinks'])
def send_links(message):
    bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKz1llYKkziE_GRF6Bncg4mYz3JssaIwACuz4AAoysCEsICzqI7ZUNFDME")
    poem_text = "Полезные ссылки"
    bot.send_message(message.chat.id, poem_text)
    #Создаем кнопку, которая появится только после сообщения и будет хранить ссылку на сайт
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    button1_url = telebot.types.InlineKeyboardButton("Одноклассники", url='https://ok.ru/group/55933980573950')
    button2_url = telebot.types.InlineKeyboardButton("VK1", url='https://vk.com/yugraecology')
    button3_url = telebot.types.InlineKeyboardButton("VK2", url='https://vk.com/eco4u2')
    button4_url = telebot.types.InlineKeyboardButton("Телеграмм", url='https://t.me/yugraecology')
    keyboard.add(button1_url,button2_url,button3_url,button4_url)
    bot.send_message(message.chat.id,  'Нажми, если хочешь знать больше', reply_markup=keyboard)


@bot.message_handler(content_types=["text"])
def answer(message):
    if message.text.strip() == 'Виды сырья':
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard= True)
        button1 = types.KeyboardButton("Стекло")
        button2 = types.KeyboardButton("Мукулатура")
        button3 = types.KeyboardButton("Пластик")
        back = types.KeyboardButton("🔙Назад")
        keyboard.add(button1,button2,button3, back)
        bot.send_message(message.chat.id, 'Виды сырья', reply_markup=keyboard)

    elif message.text.strip() == 'Мукулатура':
        send_flourpaper(message)
        
    elif message.text.strip() == 'Стекло':
        send_glass(message)

    elif message.text.strip() == 'Пластик':
        send_plastic(message)

    
    elif message.text.strip() == 'Пунткы приема':
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEKy9FlXdMxsy76z2FLfYAPyk9pZjuq_wACjzYAAv5I8Er5bkGnIlXybzME")
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard= True)
        button1 = types.KeyboardButton("Ханты-Мансийск")
        button2 = types.KeyboardButton("Сургут")
        button3 = types.KeyboardButton("Нижневартовск")
        back = types.KeyboardButton("🔙Назад")
        keyboard.add(button1,button2,button3, back)
        bot.send_message(message.chat.id, 'Пунткы приема', reply_markup=keyboard)

    elif message.text.strip() == 'Ханты-Мансийск':
        send_khanty(message)

    elif message.text.strip() == 'Сургут':
        send_surgut(message)

    elif message.text.strip() == 'Нижневартовск':
        send_nizhnevartovsk(message)



    elif message.text.strip() == 'Обучающие материалы':
        send_platform(message)

    elif message.text.strip() == '5 причин сортировать отходы':
        send_reason(message)

    elif message.text.strip() == 'Эко урок':
        send_lesson(message)

    elif message.text.strip() == 'Полезные ссылки':
        send_links(message)

    elif message.text.strip() == '🔙Назад':
        keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True, one_time_keyboard = False)
        button1 = types.KeyboardButton("Пунткы приема")
        button2 = types.KeyboardButton("Обучающие материалы")
        button3 = types.KeyboardButton("Полезные ссылки")
        button4 = types.KeyboardButton("Виды сырья")
        button5 = types.KeyboardButton("5 причин сортировать отходы")
        button6 = types.KeyboardButton("Эко урок")
        #Не забываем добавить созданные кнопки в пустой шаблон
        keyboard.add(button1,button2,button3,button4,button5,button6)
        bot.send_message(message.chat.id, 'назад', reply_markup=keyboard)
    
    else:
        bot.send_message(message.chat.id, 'Я не могу прочитать, что вы написали! введите новый запрос корректно, пожалуйста)')

#Чтобы программа работы все время
bot.polling()
